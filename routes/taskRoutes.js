// Contain all task endpoints for our applications

const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskControllers')

// route for getting all tasks
router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// route for deleting task

router.delete('/deleteTask/:id', (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


// Activity

// 1. Create a route for getting a specific task

router.get('/:id', (req, res) => {

	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


// 5. Create a route for changing the status of a task to "complete"

router.put('/:id/complete', (req, res) => {

	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

module.exports = router